package com.google.code.ihtika;

import java.util.HashMap;
import khartn.khartnosgiwrapper.OSGiLoader;

public class Starter {

    public static void main(String[] args) {
        OSGiLoader loader = new OSGiLoader();
        HashMap<String, Object> argsLocal = new HashMap<String, Object>();
        argsLocal.put("showDefaultMessageOnStart", false);
        argsLocal.put("customMessageOnStart", "Starting IhtikaClient");
        argsLocal.put("showCustomMessageOnStart", true);
        argsLocal.put("pathToFelixCache", "target/cache");
        argsLocal.put("InitialBundlesDir", "I_Bundles/Stage_700");

        HashMap<String, String> configFelix = new HashMap<String, String>();
        configFelix.put("org.osgi.framework.system.packages.extra", "com.google.code.ihtika");
        
        argsLocal.put("configFelix", configFelix);
        
        loader.start(argsLocal);
    }
}