/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ihtika2.i_aboutform.service;

import org.osgi.framework.BundleContext;

/**
 *
 * @author Arthur
 */
public interface AboutFormInterface {

    public void showWindow(BundleContext context);
    public void disposeWindow();
}
