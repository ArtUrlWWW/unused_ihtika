package ihtika2.i_aboutform.service;

import ihtika2.i_aboutform.I_AboutForm;
import javax.swing.SwingUtilities;
import org.osgi.framework.BundleContext;

/**
 *
 * @author Arthur
 */
public class AboutForm implements AboutFormInterface {

    I_AboutForm AboutForm = new I_AboutForm();

    @Override
    public void showWindow(final BundleContext context) {
        SwingUtilities.invokeLater(new Runnable() {
            // This creates of the application window.
            @Override
            public void run() {
//                AboutForm = new I_AboutForm();
                AboutForm.main(context);
            }
        });
    }

    @Override
    public void disposeWindow() {
        Runnable runner = new Runnable() {
            // This disposes of the application window.
            @Override
            public void run() {
                AboutForm.stop();
            }
        };

        if (SwingUtilities.isEventDispatchThread()) {
            runner.run();
        } else {
            try {
                SwingUtilities.invokeAndWait(runner);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
