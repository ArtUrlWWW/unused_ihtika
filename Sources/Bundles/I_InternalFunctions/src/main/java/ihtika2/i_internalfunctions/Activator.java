package ihtika2.i_internalfunctions;

import ihtika2.i_internalfunctions.service.InternalFunctions;
import ihtika2.i_internalfunctions.service.InternalFunctionsInterface;
import java.util.Hashtable;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {

    @Override
    public void start(BundleContext context) throws Exception {
        Hashtable<String, String> props = new Hashtable<String, String>();
        props.put("Funct", "IFuncts");
        context.registerService(InternalFunctionsInterface.class.getName(), new InternalFunctions(), props);
    }

    @Override
    public void stop(BundleContext context) throws Exception {
    }

}
