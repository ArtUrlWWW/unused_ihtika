/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ihtika2.mainform.service;

import org.osgi.framework.BundleContext;

/**
 *
 * @author Arthur
 */
public interface MainFormInterface {

    public void showWindow();
    public void disposeWindow();
    public void sendContext(BundleContext context);
}
