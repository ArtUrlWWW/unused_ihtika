package ihtika2.mainform;

import com.google.code.ihtika.Vars.Ini;
import ihtika2.i_aboutform.service.AboutFormInterface;
import ihtika2.i_internalfunctions.service.InternalFunctionsInterface;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import javax.swing.JFrame;
import khartn.updater.service.UpdaterInt;
import org.apache.log4j.*;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class IC_MainForm extends JFrame {

    private static IC_MainForm localFrame = null;
    BundleContext context;
    InternalFunctionsInterface InternalFunctions;

    /**
     * Creates new form IC_MainForm
     */
    public IC_MainForm(BundleContext context) {
        initComponents();
        PropertyConfigurator.configure(props());
        Ini.logger = Logger.getRootLogger();
        this.context = context;

        try {
            URL configURL = context.getBundle().getResource("ihtika2/mainform/favicon_1.jpg");
            if (configURL != null) {
                InputStream input = configURL.openStream();
                try {
                    BufferedImage qqq;
                    qqq = javax.imageio.ImageIO.read(input);
                    this.setIconImage(qqq); // NOI18N
                } finally {
                    input.close();
                }
            }

        } catch (IOException ex) {
            Ini.logger.fatal("Error on adding ico: ", ex);
        }

        try {
            ServiceReference[] refs = context.getServiceReferences(
                    InternalFunctionsInterface.class.getName(), "(Funct=IFuncts)");
            if (refs == null) {
                System.out.println("Not Found IFuncts on init");
            } else {
                InternalFunctions = (InternalFunctionsInterface) context.getService(refs[0]);

                InternalFunctions.initDB();

                MainThread MainThreadLocal = new MainThread(context);
                MainThreadLocal.start();
            }
        } catch (Exception ex) {
            Ini.logger.fatal("Error on IFuncts init", ex);
        }


    }

    public IC_MainForm() {
//
//        if (Ini.goUpdate) {
//            JDialog updateDialog = null;
//            if (updateDialog == null) {
//                updateDialog = new UpdateIhtikaClient(null);
//            }
//            IhtikaClientApp.getApplication().show(updateDialog);
//
//        } else {
////            JOptionPane.showMessageDialog(null, "Ваша копия программы устарела"
////                    + "На http://ihtik.lib.ru/ ...", "LectorInstaller", 1);
////            System.exit(0);
//            show(new IhtikaClientView(this));
//        } 
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jProgressBar1 = new javax.swing.JProgressBar();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();

        setIconImages(null);

        jLabel1.setText("jLabel1");

        jButton1.setText("jButton1");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("jButton2");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jMenu1.setText("Главное меню");

        jMenuItem2.setText("О программе");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuItem1.setText("Выход");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        menuBar.add(jMenu1);

        jMenu2.setText("Справка");

        jMenuItem3.setText("Как пользоваться программой");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);

        menuBar.add(jMenu2);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 626, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(260, 260, 260)
                                .addComponent(jButton1))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(57, 57, 57)
                                .addComponent(jButton2)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(36, 36, 36)
                .addComponent(jButton1)
                .addGap(103, 103, 103)
                .addComponent(jButton2)
                .addContainerGap(367, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed

        for (Bundle qqq : context.getBundles()) {
            if ("ihtika2.I_AboutForm".equals(qqq.getSymbolicName())) {
                System.out.println("I_AboutForm state: " + qqq.getState());
            }
        }

        try {
            ServiceReference[] refs = context.getServiceReferences(
                    AboutFormInterface.class.getName(), "(Funct=AboutForm)");
            if (refs == null) {
                System.out.println("Not Found AboutForm on show");
            } else {
                AboutFormInterface AboutForm = (AboutFormInterface) context.getService(refs[0]);
                AboutForm.showWindow(context);
            }
        } catch (Exception ex) {
            Ini.logger.fatal("Error on showing AboutForm", ex);
        }

    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        // TODO add your handling code here:
        InternalFunctions.launchBrowser("code.google.com/p/ihtika/wiki/HowToUse");
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
//        ServiceReference ref = context.getServiceReference(PackageAdmin.class.getName());
//        PackageAdmin pa = (PackageAdmin) context.getService(ref);
//        pa.refreshPackages(context.getBundles());
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try {
            ServiceReference[] refs = context.getServiceReferences(
                    UpdaterInt.class.getName(), "(Funct=*)");
            if (refs == null) {
                System.out.println("Not Found UpdaterInt on demand");
            } else {
                UpdaterInt UpdaterLocal = (UpdaterInt) context.getService(refs[0]);
                UpdaterLocal.update();
            }
        } catch (Exception ex) {
            Ini.logger.fatal("Error on showing AboutForm", ex);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

//    public void main() {
    public void main(final BundleContext context) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception ex) {
            Ini.logger.fatal("Error in MainForm: ", ex);
        }
        //</editor-fold>

//        if (args.length>0 && (args[0].equals("update") || args[0].equals("updatesvn"))) {
//            Ini.updateType = args[0];
//            Ini.goUpdate = true;
//        }

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                localFrame = new IC_MainForm(context);
                localFrame.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        try {
                            //  System.exit(0);
                            System.out.println("Closing main window");
                            context.getBundle(0).stop();
                        } catch (Exception ex) {
                            Ini.logger.fatal("Error on exiting: ", ex);
                        }
                    }
                });
                localFrame.setVisible(true);
            }
        });
    }

    public void stop() {
        if (localFrame != null) {
            localFrame.setVisible(false);
            localFrame.dispose();
            localFrame = null;
        }
    }

    private static Properties props() {
        Properties props = new Properties();
        props.put("log4j.rootLogger", "INFO, R");
        props.put("log4j.appender.R",
                "org.apache.log4j.DailyRollingFileAppender");
        props.put("log4j.appender.R.File", "logs/IhtikaClient.log");
        props.put("log4j.appender.R.Append", "true");
        props.put("log4j.appender.R.Threshold", "INFO");
        props.put("log4j.appender.R.DatePattern", "'.'yyyy-MM-dd");
        props.put("log4j.appender.R.layout", "org.apache.log4j.PatternLayout");
        props.put("log4j.appender.R.layout.ConversionPattern",
                //"%d{HH:mm:ss,SSS} %c - %m%n");
                //"[%5p] %d{yyyy-MM-dd mm:ss} (%F:%M:%L)%n%m%n%n");
                "[%5p] %d{yyyy-MM-dd mm:ss} %c (%F:%M:%L)%n%m%n");
        return props;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    public static javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JMenuBar menuBar;
    // End of variables declaration//GEN-END:variables
}
