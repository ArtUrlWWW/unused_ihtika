package ihtika2.mainform;

import com.google.code.ihtika.Vars.Ini;
import ihtika2.mainform.service.MainFormInterface;
import java.util.HashSet;
import java.util.Hashtable;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.wiring.FrameworkWiring;

public class Activator implements BundleActivator {

    @Override
    public void start(BundleContext context) throws Exception {
        Hashtable<String, String> props = new Hashtable<String, String>();
        props.put("Funct", "MainForm");
        context.registerService(MainFormInterface.class.getName(), new MainForm(), props);

        ServiceReference[] refs = context.getServiceReferences(
                MainFormInterface.class.getName(), "(Funct=MainForm)");
        if (refs == null) {
            System.out.println("Not Found MainForm on start");
        } else {
            MainFormInterface MainForm = (MainFormInterface) context.getService(refs[0]);
            MainForm.sendContext(context);
            MainForm.showWindow();
        }

//        System.out.println("Stage 1");
//        
//        int x = 0;
//            for (Bundle qqq : context.getBundles()) {
//                if (x < 1) {
//                    HashSet<Bundle> bundles;
//                    bundles = new HashSet<Bundle>();
//                    bundles.add(qqq);
//                    HashSet<Bundle> depends = (HashSet<Bundle>) context.getBundle(0).adapt(FrameworkWiring.class).getDependencyClosure(bundles);
//                    System.out.println("***---");
//                    System.out.println(qqq.getSymbolicName());
//                    System.out.println("***+++");
//                    for (Bundle depends1 : depends) {
//                        System.out.println(depends1.getSymbolicName());
//                    }
//                }
////                x++;
//            }
        
//        OSGiLoader.getBundles(context.getBundles());
        //
        //        Dictionary headers = context.getBundle().getHeaders();
        //
        //        String exportedPacksStr = (String) headers.get("Export-Package");
        //        String importedPacksStr = (String) headers.get("Import-Package");
        //
        //        System.out.println(importedPacksStr);
        //        System.out.println(importedPacksStr);
        //
        //        String[] importedPacksArray = importedPacksStr.split(",");
        //        ServiceReference ref = context.getServiceReference(PackageAdmin.class.getName());
        //        PackageAdmin pa = (PackageAdmin) context.getService(ref);
        //        pa.refreshPackages(context.getBundles());
        //        HashSet<Bundle> bundles;
        //        bundles = new HashSet<Bundle>();
        //        bundles.add(context.getBundle());
        //        Bundle sddfsdsdf=context.getBundle();
        //        HashSet<Bundle> depends = (HashSet<Bundle>) sddfsdsdf.adapt(FrameworkWiring.class).getDependencyClosure(bundles);
        //        for (Bundle depends1 : depends) {
        //            System.out.println(depends1.getSymbolicName());
        //        }
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        try {
            // NOTE: The service is automatically unregistered.

            ServiceReference[] refs = context.getServiceReferences(
                    MainFormInterface.class.getName(), "(Funct=MainForm)");
            if (refs == null) {
                System.out.println("Not Found MainForm on Stop");
            } else {
                MainFormInterface MainForm = (MainFormInterface) context.getService(refs[0]);
                MainForm.disposeWindow();
            }
        } catch (InvalidSyntaxException ex) {
            Ini.logger.fatal("Error on stopping MainForm", ex);
        }
    }
}
