package ihtika2.mainform;

import ihtika2.mainform.service.MainFormInterface;
import javax.swing.SwingUtilities;
import org.osgi.framework.BundleContext;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class MainForm implements MainFormInterface {

    BundleContext context;

    @Override
    public void sendContext(BundleContext context) {
        this.context = context;
    }
    
    IC_MainForm MainForm;

    @Override
    public void showWindow() {        
        SwingUtilities.invokeLater(new Runnable() {
            // This creates of the application window.
            @Override
            public void run() {
                MainForm = new IC_MainForm();
                MainForm.main(context);
            }
        });
    }

    @Override
    public void disposeWindow() {
        Runnable runner = new Runnable() {
            // This disposes of the application window.
            @Override
            public void run() {
                MainForm.stop();
            }
        };

        if (SwingUtilities.isEventDispatchThread()) {
            runner.run();
        } else {
            try {
                SwingUtilities.invokeAndWait(runner);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
