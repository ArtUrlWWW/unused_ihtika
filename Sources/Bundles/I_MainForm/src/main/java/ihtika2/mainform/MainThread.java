package ihtika2.mainform;

import com.google.code.ihtika.Vars.Ini;
import ihtika2.i_internalfunctions.service.InternalFunctionsInterface;
import ihtika2.i_luceneindexhandler.service.SourceLocalInt;
import net.htmlparser.jericho.Source;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class MainThread extends Thread {

    BundleContext context;

    public MainThread(BundleContext context) {
        this.context = context;
    }

    @Override
    public void run() {
        IC_MainForm.jLabel1.setText("Проверка обновлений. Обновление "
                + "может длиться более 40 секунд.");

        try {
            ServiceReference[] refs = context.getServiceReferences(
                    SourceLocalInt.class.getName(), "(Funct=SourceLocal)");
            if (refs == null) {
                System.out.println("Not Found IFuncts on init");
            } else {
                SourceLocalInt sourceLocal = (SourceLocalInt) context.getService(refs[0]);
                Source source = sourceLocal.get_source("http://ihtik.lib.ru/", false);
//                System.out.println(source);

            }
        } catch (Exception ex) {
            Ini.logger.fatal("Error on IFuncts init", ex);
        }

        // Получаем главную страницу

//        Update updateDistr = new Update();
//        updateDistr.update();
//
//        SVNClass lSVNClass = new SVNClass();
//        lSVNClass.updateDistr();
//
//        try {
//                    InternalFunctions InternalFunctionsLocal = new InternalFunctions();
//
//            InternalFunctionsLocal.Process();
//            Ini.iterateProgress = false;
//            IhtikaClientView.jLabel1.setText("Данные успешно загружены.");
//
//        } catch (Exception ex) {
//            Ini.logger.fatal("Error: ", ex);
//        }
    }
}
