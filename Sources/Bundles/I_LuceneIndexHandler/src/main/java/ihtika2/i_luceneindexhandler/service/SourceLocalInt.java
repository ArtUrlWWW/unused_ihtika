package ihtika2.i_luceneindexhandler.service;

import net.htmlparser.jericho.Source;

/**
 *
 * @author Arthur
 */
public interface SourceLocalInt {
    public Source get_source(String pageURL, Boolean checkInBase);
}
