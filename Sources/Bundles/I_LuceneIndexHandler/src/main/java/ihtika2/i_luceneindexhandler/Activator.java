package ihtika2.i_luceneindexhandler;

import ihtika2.i_luceneindexhandler.service.SourceLocal;
import ihtika2.i_luceneindexhandler.service.SourceLocalInt;
import java.util.Hashtable;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {

    @Override
    public void start(BundleContext context) throws Exception {
                Hashtable<String, String> props = new Hashtable<String, String>();
        props.put("Funct", "SourceLocal");
        context.registerService(SourceLocalInt.class.getName(), new SourceLocal(), props);
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

}
