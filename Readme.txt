Данная программа, именуемая Ihtika Client, распространяется по принципу "AS IS", автор не берёт на себя ответственности за возможный ущерб, который может быть причинён при использовании данной программы - вы используете её на свой страх и риск.

Ihtika Client частично основывается на функционале JDownloader (http://jdownloader.org/), а именно используется JAC. Исходные коды JDownloader вы можете взять с сайта http://dev.jdownloader.org/ .

*************************************************

The program, called Ihtika Client, distributed on a "AS IS", author assumes no liability for any damage that may occur when using this program - you use it at your own risk.

Ihtika Client is based in part on functional JDownloader (http://jdownloader.org/), namely using JAC. Source code of the JDownloader you can take from the site http://dev.jdownloader.org/ .