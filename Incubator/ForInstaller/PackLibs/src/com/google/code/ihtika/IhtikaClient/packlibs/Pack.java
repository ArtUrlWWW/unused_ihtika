package com.google.code.ihtika.IhtikaClient.packlibs;

import com.google.code.ihtika.IhtikaClient.packlibs.Vars.Ini;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.util.Map;
import java.util.jar.JarFile;
import java.util.jar.Pack200;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class Pack {

    private final String PACKEXT = ".pack";
    private GZip gZip = new GZip();

    private void recursivePack(File currentDir) {
        Pack200.Packer packer = Pack200.newPacker();
        Map p = packer.properties();
        p.put(packer.EFFORT, "9");

        FileFilter filefilter = new FileFilter() {

            @Override
            public boolean accept(File file) {
                if ((file.getName().endsWith(".jar")
                        || file.getName().endsWith(".jar.svn-base"))
                        && !file.getName().equals("PackLibs.jar")) {
                    return true;
                }
                return false;
            }
        };

        for (File lib : currentDir.listFiles(filefilter)) {
            try {
                throw new Exception();
            } catch (Exception ex) {
//                System.out.println("Error with processing file: " + ex.getMessage());
            }
            try {
                System.out.println("Processing file " + lib.getAbsolutePath());
                JarFile jarFile = new JarFile(lib);
                FileOutputStream fos = new FileOutputStream(lib.getAbsolutePath() + PACKEXT);
                packer.pack(jarFile, fos);
                jarFile.close();
                fos.close();
                gZip.gzipFile(lib.getAbsolutePath() + PACKEXT, lib.getAbsolutePath() + PACKEXT + ".gz");
                new File(lib.getAbsolutePath() + PACKEXT).delete();
                lib.delete();
            } catch (Exception ex) {
//                System.out.println("Error with processing file: " + lib.getAbsolutePath()
//                        + " " + ex.getMessage());
//                ex.printStackTrace();
            }
        }
        for (File dir : currentDir.listFiles()) {
            if (dir.isDirectory()) {
                recursivePack(dir);
            }
        }


    }

    public void pack() {
        File libsDir = new File(Ini.pathToLibs);
        libsDir = new File(libsDir.getAbsolutePath());
        recursivePack(libsDir);

        libsDir = new File(Ini.pathToDistributive);
        libsDir = new File(libsDir.getAbsolutePath());
        recursivePack(libsDir);
    }
}
