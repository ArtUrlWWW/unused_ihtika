package com.google.code.ihtika.IhtikaClient.packlibs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class CopyFile {

    public void copyfile(File src, File dest) {
        if (!src.getName().equals("Db")
                && !src.getName().equals("lucene")
                && !src.getName().equals("Distr")
                && !src.getName().equals("Download_Master_Portable")
                && !src.getName().equals("logs")
                && !src.getName().equals("PackLibs.jar")
                && !src.getName().equals("PackLibs.bat")
                && !src.getName().equals("tmp")) {
            if (src.isDirectory()) {

                //if directory not exists, create it
                if (!dest.exists()) {
                    dest.mkdir();
                    System.out.println("Directory copied from "
                            + src + "  to " + dest);
                }

                //list all the directory contents
                String files[] = src.list();

                for (String file : files) {
                    //construct the src and dest file structure
                    File srcFile = new File(src, file);
                    File destFile = new File(dest, file);
                    //recursive copy
                    copyfile(srcFile, destFile);
                }

            } else {
                InputStream in = null;
                try {
                    in = new FileInputStream(src);
                    OutputStream out = new FileOutputStream(dest);
                    byte[] buffer = new byte[1024];
                    int length;
                    //copy the file content in bytes
                    while ((length = in.read(buffer)) > 0) {
                        out.write(buffer, 0, length);
                    }
                    in.close();
                    out.close();
                    System.out.println("File copied from " + src + " to " + dest);
                } catch (Exception ex) {
                    Logger.getLogger(CopyFile.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    try {
                        in.close();
                    } catch (IOException ex) {
                        Logger.getLogger(CopyFile.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }
}