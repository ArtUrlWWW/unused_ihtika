package com.google.code.ihtika.IhtikaClient.packlibs;

import java.io.File;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class PackLibs {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (args[0].equals("pack")) {
            System.out.println("Packing files...");
            Pack pack = new Pack();
            pack.pack();
        }
        if (args[0].equals("unpack")) {
            System.out.println("Unpacking files...");
            UnPack unPack = new UnPack();
            unPack.unpack();
        }
        if (args[0].equals("unpackAndCopy")) {
            System.out.println("Unpacking files...");
            UnPack unPack = new UnPack();
            unPack.unpack();
            System.out.println("Copy files...");
            CopyFile copyFile = new CopyFile();
            copyFile.copyfile(new File("."), new File("Distr/"));
        }
    }
}
