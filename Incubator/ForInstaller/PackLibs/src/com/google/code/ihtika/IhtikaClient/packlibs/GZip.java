package com.google.code.ihtika.IhtikaClient.packlibs;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class GZip {

    /**
     * Чтение файла
     * @param fileName - путь к файлу, для чтения
     * @return - данные из файла
     */
    public byte[] readFile(String fileName) throws Exception {
        InputStream in = new FileInputStream(fileName);
        byte[] data = new byte[in.available()];
        in.read(data);
        in.close();
        return data;
    }

    /**   
     * Сжатие массива байт data и сохранение в toFileName
     * @param
    data 
    - массив байт, предназначенный для сжатия
     * @param
    toFileName 
    - путь к файлу , в который сохраняем
    
     */
    public void gzipArray(byte[] data, String toFileName) throws Exception {
        OutputStream out =
                new GZIPOutputStream(new FileOutputStream(toFileName));
        out.write(data);
        out.flush();
        out.close();
    }

    /**
     * Применение операций чтения, а затем сжатия
     * @param fromFileName - путь к файлу, которой сжимаем
     * @param toFileName - путь к файлу, в который сохраняем
     */
    public void gzipFile(String fromFileName, String toFileName)
            throws Exception {
        byte[] array = readFile(fromFileName);
        gzipArray(array, toFileName);
    }

    public String unpack(String a) {
        try {
            GZIPInputStream in = new GZIPInputStream(new BufferedInputStream(new FileInputStream(a)));
            String outFilename = a.substring(0, a.lastIndexOf("."));
            OutputStream out = new FileOutputStream(outFilename);

            byte[] buf = new byte[1024];

            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }

            in.close();
            out.close();
            return outFilename;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
