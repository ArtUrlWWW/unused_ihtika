package com.google.code.ihtika.IhtikaClient.packlibs;

import com.google.code.ihtika.IhtikaClient.packlibs.Vars.Ini;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.util.jar.JarOutputStream;
import java.util.jar.Pack200;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class UnPack {

    private final String PACKEXT = ".pack";
    private GZip gZip = new GZip();

    private void recursiveUnPack(File currentDir) {

        FileFilter filefilter = new FileFilter() {

            @Override
            public boolean accept(File file) {
                if (file.getName().endsWith(".pack.gz")) {
                    return true;
                }
                return false;
            }
        };

        for (File lib : currentDir.listFiles()) {
            if (lib.isDirectory()) {
                recursiveUnPack(lib);
            }
        }

        for (File lib : currentDir.listFiles(filefilter)) {
            try {
                String filePath = lib.getAbsolutePath();
                filePath = gZip.unpack(filePath);
                if (filePath != null) {
                    System.out.println("Unpacking file " + lib.getAbsolutePath());
                    File f = new File(filePath);
                    filePath = filePath.substring(0, filePath.lastIndexOf("."));
                    // Write out to current directory, jtreg will setup a scratch area
                    JarOutputStream jostream = new JarOutputStream(new FileOutputStream(filePath));
                    // Unpack the files
                    Pack200.Unpacker unpacker = Pack200.newUnpacker();
                    // Call the unpacker
                    unpacker.unpack(f, jostream);
                    // Must explicitly close the output.
                    jostream.close();

                    f.delete();
                    lib.delete();
                } else {
                    System.out.println("Error!");
                }
            } catch (Exception ex) {
//                System.out.println("Error with processing file: " + lib.getAbsolutePath());
//                ex.printStackTrace();
            }
        }

    }

    public void unpack() {
        File libsDir = new File(Ini.pathToLibs);
        recursiveUnPack(libsDir);

        libsDir = new File(Ini.pathToDistributive);
        recursiveUnPack(libsDir);
    }
}
